import os

class LoginCredential():
	def __init__(self):
		# Will retrieve a username and password from the environment, given name
		self.username = os.environ['UF_USERNAME']
		self.password = os.environ['UF_PASSWORD']
		self.email = os.environ['RECEIPT_EMAIL']
