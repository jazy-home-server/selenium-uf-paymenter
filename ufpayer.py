from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class UFPayer():
	# driver: is a selenium webdriver
	# credential: is a LoginCred object
	def __init__(self, driver, credential):
		self.credential = credential
		self.driver = driver

	# amount: Should be a float in dollars
	# returns the current balance (Before payment)
	def pay(self, amount, dryrun = False):
		browser = self.driver
		wait = WebDriverWait(browser, 10)

		browser.get('https://one.uf.edu')

		# Click the main login button
		ele = browser.find_element_by_xpath('//button[@ng-click="$ctrl.login()"]')
		ele.click()

		# Fill out the login form and submit
		ele = browser.find_element_by_id('username')
		ele.send_keys(self.credential.username)

		ele = browser.find_element_by_id('password')
		ele.send_keys(self.credential.password + Keys.RETURN)

		assert browser.title == 'ONE.UF', 'One UF login failed'

		# Go to the campus finances page
		browser.get('https://one.uf.edu/campusfinances/')

		assert 'Campus Finances' in browser.title, 'Campus finances page failed to load'

		# Get the balance
		ele = wait.until(EC.presence_of_element_located((By.CLASS_NAME, 'balance-total')))
		balance = ele.text

		# Go to payment portal
		browser.get('http://my.ufl.edu/psp/ps/EMPLOYEE/CAMP/c/UF_SF_MENU.UF_SFCN_PAYMENT.GBL')

		assert 'CashNet' in browser.title, 'Cash Net page failed to load'

		ele = wait.until(EC.presence_of_element_located((By.ID, 'UF_SFCN_PAY_VW_ITEM_AMT$0')))
		assert balance == ele.text, 'Balance on One.UF doesnt match CashNet'

		ele = browser.find_element_by_id('UF_SFCN_PAY_WRK_UF_PAYMENT_TYPE')
		ele.click()
		ele = browser.find_element_by_id('UF_SFCN_PAY_WRK_CHECK_BOX')
		ele.click()
		ele = browser.find_element_by_id('UF_SFCN_PAY_WRK_URL')
		ele.click()

		wait.until(EC.title_contains('UF ePayment'))
		#assert 'UF ePayment' in browser.title
		#assert balance in browser.page_source # Ok bud...

		ele = browser.find_element_by_id('dgBalances_ctl02_hlBalTitle')
		ele.click()

		wait.until(EC.title_contains('UF ePayment'))
		#assert 'UF ePayment' in browser.title
		# Enter payment amount
		ele = browser.find_element_by_id('txtPrice')
		ele.clear()
		ele.send_keys(amount)

		# Add to cart, lol
		ele = browser.find_element_by_id('btAddToCart')
		ele.click()

		# Confirm amount
		ele = browser.find_element_by_id('lblGrandTotalAmount')
		assert ele.text[1:] == amount, 'Amount being paid differs from amount inputted!'

		ele = browser.find_element_by_name('btCheckout')
		ele.click()

		# Confirm (Might be conditional!!) (Might throw NoSuchElementException)
		try:
			ele = browser.find_element_by_id('cbAgree')
			ele.click()
			ele = browser.find_element_by_id('btnProceed')
			ele.click()
		except NoSuchElementException:
			pass # Simply continue since this page didnt appear apparently
		finally:
			wait.until(EC.title_contains('Select Payment Type'))

		# Select payment method
		#assert 'Select Payment Type' in browser.title
		ele = browser.find_element_by_id('ucPayOptions_ddSavedPayments')
		ele = Select(ele)
		options = ele.options
		for option in options:
			if 'FLCU' in option.get_attribute('value'):
				ele.select_by_value(option.get_attribute('value'))
				break

		ele = browser.find_element_by_id('btnProceed')
		ele.click()

		# Final confirm and send me an email :D
		ele = browser.find_element_by_id('txtEmailAddr')
		ele.clear()
		ele.send_keys(self.credential.email)

		ele = browser.find_element_by_id('btnOK')

		if dryrun == 'false':
			ele.click() # WARNING ACTUAL TRANSACTION ACTION
		else:
			return balance

		# Done
		wait.until(EC.title_contains('Receipt'), 'Receipt page was never reached!')
		#assert 'Receipt' in browser.title
		assert amount == browser.find_element_by_id('lblPaidItemTotal').text[1:], 'Final amount paid differs from original amount!'

		return balance
