from logincredential import LoginCredential
from ufpayer import UFPayer
from selenium import webdriver
from dotenv import load_dotenv
import os
from datetime import datetime

# Load environment variables
load_dotenv()

dryrun = os.getenv('DRY_RUN', 'false')
storeScreenshot = os.getenv('STORE_SCREENSHOT', 'false')
screenshotDirectory = os.getenv('SCREENSHOTS_DIRECTORY', './screenshots/')
amount = os.getenv('AMOUNT', '0.05')
driverURL = os.getenv('WEBDRIVER', 'http://192.168.0.100:4445/wd/hub')

dryrunString = '' if dryrun == 'false' else 'Dry Run:'

credential = LoginCredential()
browser = webdriver.Remote(driverURL, {'browserName': 'chrome'})

payer = UFPayer(browser, credential)
try:
	balance = payer.pay(amount, dryrun)
	print(dryrunString + 'Remaining balance: ' + balance + ' - ' + amount)
except Exception as e:
	print(e)
finally:
	if storeScreenshot == 'false':
		pass
	else:
		screenshotName = datetime.now().strftime('%m_%d_%Y-%H_%M.png')
		browser.save_screenshot(screenshotDirectory + screenshotName)
		print('Screenshot stored: ' + screenshotName)
	browser.quit()
