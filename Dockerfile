FROM python:3

RUN mkdir -p /app/
WORKDIR /app/

COPY requirements.txt /app/
RUN pip install -r requirements.txt

COPY . /app/
RUN mkdir /app/screenshots

CMD ["/usr/local/bin/python", "app.py"]
